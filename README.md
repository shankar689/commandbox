# USAGE

- data.sh is used to pass the required parameters for container creation.
- create.sh is used to create a container.
- destroy.sh is used to remove a pirticular container.
- list.sh to list the existing containers.
